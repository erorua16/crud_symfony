<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Manga;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;

class MangaFormController extends AbstractController
{
    #[Route('/manga/form', name: 'app_manga_form')]
    public function form(ManagerRegistry $doctrine, Request $request): Response
    {
        // creates a task object and initializes some data for this example
        $manga = new Manga();
        $entityManager = $doctrine->getManager();
        $mangaData = $entityManager->getRepository(Manga::class);

        $form = $this->createFormBuilder($manga)
            ->add('title', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Create Manga'])
            ->getForm();


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            
            $addManga = $form->getData();

            if(!empty($mangaData->findOneBy(['title' => $addManga->getTitle()]) == false)){
                $entityManager->persist($addManga);
                $entityManager->flush();

                return $this->redirect('/manga/all');
            };

            throw $this->createNotFoundException(
                'This manga already exists'
            );
    
        }

        return $this->renderForm('manga/form.html.twig', [
            'form' => $form,
        ]);

    }
}
