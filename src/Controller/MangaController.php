<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;


use App\Entity\Manga;
use Doctrine\Persistence\ManagerRegistry;

class MangaController extends AbstractController
{
    // #[Route('/manga', name: 'app_manga')]

    // public function createManga(ManagerRegistry $doctrine): Response
    // {
    //     $entityManager = $doctrine->getManager();

    //     $manga = new Manga();
    //     $manga->setTitle('Monster');

    //     // tell Doctrine you want to (eventually) save the manga (no queries yet)
    //     $entityManager->persist($manga);

    //     // actually executes the queries (i.e. the INSERT query)
    //     $entityManager->flush();

    //     return new Response('Saved new manga with id '.$manga->getId());
    // }

    #[Route('/manga/all', name: 'app_manga')]
    public function showAll(ManagerRegistry $doctrine): Response
    {
        $manga = $doctrine->getRepository(Manga::class)->findAll();

        if (!$manga) {
            throw $this->createNotFoundException(
                'No manga found'
            );
        }

        // return new Response('Check out this great manga: '.$manga->getTitle());

        return $this->render('home/manga.html.twig', ['manga' => $manga]);
    }

    #[Route('/manga/edit', name: 'app_manga_edit')]
    public function update(ManagerRegistry $doctrine, Request $request): Response
    {
        $id = $request->get('id');
        $entityManager = $doctrine->getManager();
        $entityManager->getRepository(Manga::class);
        $manga = $entityManager->getRepository(Manga::class);
        
        if (!$manga->find($id)) {
            throw $this->createNotFoundException(
                'No manga found for id '.$id
            );
        }
        $form = $this->createFormBuilder($manga->find($id))
        ->add('title', TextType::class)
        ->add('save', SubmitType::class, ['label' => 'Edit Manga'])
        ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $editManga = $form->getData();

            $checkExist = $manga->findOneBy(['title' => $editManga->getTitle()]);
            
            if(!empty( $checkExist == false || $checkExist->getId() == $editManga->getId())){
                $entityManager->persist($editManga);
                $entityManager->flush();

                return $this->redirect('/manga/all');
            };

            throw $this->createNotFoundException(
                'This manga already exists'
            );
            
        }

        return $this->renderForm('home/mangaEditForm.html.twig', [
            'form' => $form,
        ]);
       
    }

    #[Route('/manga/delete', name: 'app_manga_delete')]
    public function delete(ManagerRegistry $doctrine, Request $request): Response
    {
        $id = $request->get('id');
        $entityManager = $doctrine->getManager();
        $manga = $entityManager->getRepository(Manga::class)->find($id);

        $entityManager->remove($manga);
        $entityManager->flush();

        return $this->redirectToRoute('app_manga', [
            'id' => $manga->getId()
        ]);
    }
    
}
