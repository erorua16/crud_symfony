# crud_symfony



## Getting started

1. clone project or install zip
2. inside the root directory, run `composer install`
3. edit the `.env-example` file with your information
4. rename `.env-example` to `.env` 
5. To open server run in terminal `symfony server:start`
